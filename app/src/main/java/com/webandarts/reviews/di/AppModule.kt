package com.webandarts.reviews.di

import org.koin.core.qualifier.named


/**
 * ScopeID for the current logged in scope
 */
const val REVIEWER_SCOPE_ID = "REVIEWER_SCOPE_ID"

/**
 * Qualifier for the logged in scope
 */
val REVIEWER_SCOPE_QUALIFIER = named(REVIEWER_SCOPE_ID)

object AppModule {
}