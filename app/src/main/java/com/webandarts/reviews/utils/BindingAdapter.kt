package com.webandarts.reviews.utils

import android.view.View
import androidx.databinding.BindingAdapter

object BindingAdapter {

    /**
     * Sets the view as visible if true otherwise as invisible
     */
    @JvmStatic
    @BindingAdapter("visibleOrGone")
    fun visibleOrGone(view: View, visible: Boolean) {
        view.visibility = if (visible) {
            View.VISIBLE
        } else {
            View.GONE
        }
    }
}