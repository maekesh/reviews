package com.webandarts.reviews.base

import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.fragment.app.Fragment
import com.google.android.material.snackbar.Snackbar
import com.webandarts.reviews.R
import com.webandarts.reviews.di.REVIEWER_SCOPE_ID
import com.webandarts.reviews.di.REVIEWER_SCOPE_QUALIFIER
import com.webandarts.reviews.preferences.PreferenceManager
import kotlinx.coroutines.*
import org.koin.android.ext.android.inject
import org.koin.core.KoinComponent
import org.koin.core.scope.Scope
import kotlin.coroutines.CoroutineContext
import kotlin.coroutines.EmptyCoroutineContext

/**
 * Base Fragment for All fragments which can handle the [parentContext] scope
 * */
abstract class BaseFragment(private val parentContext: CoroutineContext = EmptyCoroutineContext) :
    Fragment(), KoinComponent {

    /**
     * All the child fragments should implement this to handle click listener
     * */
    abstract fun setActionListener()

//    private var errorBottomSheetFragment: ErrorViewBottomSheetFragment? = null

    protected lateinit var coroutineScope: CoroutineScope
        private set

    val viewModelScope: Scope? get() = _currentScope

    //  private val progressDialog = ProgressDialog.getInstance(false)

    private val preferenceManager: PreferenceManager by inject()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        coroutineScope =
            CoroutineScope(Dispatchers.Main + parentContext + SupervisorJob(parentContext[Job]))
    }

    override fun onDestroy() {
        coroutineScope.cancel()
        super.onDestroy()
    }

    private fun internalCreateScope(): Scope? {
        val created = getKoin().getScopeOrNull(REVIEWER_SCOPE_ID) == null
        val scope = getKoin()
            .getOrCreateScope(REVIEWER_SCOPE_ID, REVIEWER_SCOPE_QUALIFIER)

        if (created) {
            scope.declare(this)
        }

        return scope
    }

    private var _currentScope = internalCreateScope()

    /**
     * Destroy/close out a logged in scope. Should only be called when a user is logging out
     */
    fun destroyLoggedInScope() {
        _currentScope.also { scope ->
            _currentScope = null
            scope?.close()
        }
    }

    /**
     * Start a new logged in scope.  If one is already active then it is returned without creation
     */
    fun startLoggedInScope(): Scope? = internalCreateScope().apply {
        _currentScope = this
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setActionListener()
    }

    protected fun showErrorMessage(message: String, view: View) {
        try {
            val snack = Snackbar.make(
                view,
                message,
                Snackbar.LENGTH_INDEFINITE
            )
            snack.setAction(resources.getString(R.string.ok)) {
                snack.dismiss()
            }
            snack.show()
        } catch (e: Exception) {
            e.printStackTrace()
            if (isAdded) {
                Toast.makeText(requireContext(), message, Toast.LENGTH_SHORT).show()
            }
        }
    }


    protected fun getSimpleName(fragment: Fragment) : String {
        return fragment::class.java.simpleName
    }


    /* protected fun showProgress(show: Boolean) {
         try {
             if (show && !progressDialog.isAdded) {
                 progressDialog.show(childFragmentManager, "Progress")
             } else {
                 progressDialog.dismissDialog()
             }
         } catch (e: Exception) {
             e.printStackTrace()
         }
     }*/

/*    protected fun showErrorView(callback: () -> Unit?, exception: ApiFailureException) {
        try {
//            showProgress(false)

            if (exception.code == INVALID_TOKEN) {
                navigateToLogin()
            } else {
//                errorBottomSheetFragment = ErrorViewBottomSheetFragment.newInstance(callback, exception)
//                errorBottomSheetFragment?.show(childFragmentManager, "dialog")
            }
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }*/

    protected fun dismissErrorView() {
//        errorBottomSheetFragment?.dismiss()
    }

    private fun navigateToLogin() {
        Toast.makeText(
            requireContext(),
            "User session has been expired, Please Login again",
            Toast.LENGTH_SHORT
        ).show()

        preferenceManager.clearUserData()

//        requireActivity().startActivity(Intent(requireContext(), LoginActivity::class.java))
//        requireActivity().finish()
    }
}