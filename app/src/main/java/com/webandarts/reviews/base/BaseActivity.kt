package com.webandarts.reviews.base

import android.content.Context
import android.os.Bundle
import android.view.View
import android.view.inputmethod.InputMethodManager
import androidx.appcompat.app.AppCompatActivity
import com.google.android.material.snackbar.Snackbar
import com.webandarts.reviews.R
import com.webandarts.reviews.di.REVIEWER_SCOPE_ID
import com.webandarts.reviews.di.REVIEWER_SCOPE_QUALIFIER
import kotlinx.coroutines.*
import org.koin.core.scope.Scope
import org.koin.java.KoinJavaComponent.getKoin
import kotlin.coroutines.CoroutineContext
import kotlin.coroutines.EmptyCoroutineContext

abstract class BaseActivity(
    private val parentContext: CoroutineContext = EmptyCoroutineContext
) : AppCompatActivity() {
    protected lateinit var coroutineScope: CoroutineScope
        private set

    val viewModelScope: Scope? get() = _currentScope

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        coroutineScope =
            CoroutineScope(Dispatchers.Main + parentContext + SupervisorJob(parentContext[Job]))
    }

    override fun onDestroy() {
        coroutineScope.cancel()
        super.onDestroy()
    }

    private fun internalCreateScope(): Scope? {
        val created = getKoin().getScopeOrNull(REVIEWER_SCOPE_ID) == null
        val scope = getKoin()
            .getOrCreateScope(REVIEWER_SCOPE_ID, REVIEWER_SCOPE_QUALIFIER)

        if (created) {
            scope.declare(this)
        }

        return scope
    }

    private var _currentScope = internalCreateScope()

    /**
     * Destroy/close out a logged in scope. Should only be called when a user is logging out
     */
    fun destroyLoggedInScope() {
        _currentScope.also { scope ->
            _currentScope = null
            scope?.close()
        }
    }

    /**
     * Start a new logged in scope.  If one is already active then it is returned without creation
     */
    fun startLoggedInScope(): Scope? = internalCreateScope().apply {
        _currentScope = this
    }

    protected fun showErrorMessage(message: String, view: View) {
        val snack = Snackbar.make(
            view,
            message,
            Snackbar.LENGTH_INDEFINITE
        )
        snack.setAction(resources.getString(R.string.ok)) {
            snack.dismiss()
        }
        snack.show()
    }


    /**
     * To close the open keyboard from anywhere in the app
     * */
    fun closeSoftKeyboard() {
        val inputManager: InputMethodManager =
            getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
        inputManager.hideSoftInputFromWindow(
            currentFocus?.windowToken,
            InputMethodManager.SHOW_FORCED
        )
    }
}