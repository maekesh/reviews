package com.webandarts.reviews.preferences

import android.content.Context
import android.content.SharedPreferences

const val PREF_BUSINESS_SIGN_UP_DATA_MODEL = "Need to update as per the scope"

class PreferenceManager(context: Context) {

    val preferences: SharedPreferences =
        context.getSharedPreferences("prefs", Context.MODE_PRIVATE)

    /**
     * Used to store the string the values in preference
     * @param [key] preference key used to store values
     * @param [id] preference value
     * */
    fun storeValue(key: String, id: String) {
        preferences.edit().putString(key, id).apply()
    }

    /**
     * Used to get the string the values from preference
     * @param [key] preference key used to store values
     */
    fun getValue(key: String): String? =
        preferences.getString(key, "")

    private fun storeStringSet(key: String, list: MutableSet<String>) {
        val set: MutableSet<String> = HashSet()
        set.addAll(list)
        preferences.edit().putStringSet(key, set).apply()
    }

    private fun getStringSet(key: String): MutableSet<String>? =
        preferences.getStringSet(key, null)

    /**
     * Used to store the string the values in preference
     * @param [key] preference key used to store values
     * @param [id] preference value
     * */
    fun storeBooleanValue(key: String, value: Boolean) {
        preferences.edit().putBoolean(key, value).apply()
    }

    /**
     * Used to get the boolean the values from preference
     * @param [key] preference key used to store values
     */
    fun getBooleanValue(key: String): Boolean =
        preferences.getBoolean(key, false)

    /**
     * Saves object into the Preferences.
     *
     * @param `object` Object of model class (of type [T]) to save
     * @param key Key with which Shared preferences to
     **/
//    fun <T> storeModelValue(objectData: T, key: String) {
//        // Convert object to JSON String.
//        val jsonString = GsonBuilder().create().toJson(objectData)
//        // Save that String in SharedPreferences
//        preferences.edit().putString(key, jsonString).apply()
//    }

    /**
     * Clear all the user saved values
     * */
    fun clearUserData() {
        preferences.edit().putString(PREF_BUSINESS_SIGN_UP_DATA_MODEL, "").apply()
    }

    /**
     * Used to retrieve object from the Preferences.
     *
     * @param key Shared Preference key with which object was saved.
     **/
//    inline fun <reified T> getModelValue(key: String): T {
//        val value = preferences.getString(key, null)
//        return GsonBuilder().create().fromJson(value, T::class.java)
//    }
}